//
//  TaskTableCell.h
//  Student's organaizer
//
//  Created by Andrei on 19.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TaskTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *SubjectNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *TaskTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *PriorityLabel;
@property (weak, nonatomic) IBOutlet UILabel *DataLabel;

@end
