//
//  SummaryTableCell.h
//  Student's organaizer
//
//  Created by Andrei on 27.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *subjectNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;


@end
