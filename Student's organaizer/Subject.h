//
//  Subject.h
//  Student's organaizer
//
//  Created by Andrei on 12.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Professor, Semester, Task;

@interface Subject : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *class_;
@property (nonatomic, retain) NSSet *professor;
@property (nonatomic, retain) Semester *semester;
@property (nonatomic, retain) NSSet *task;
@end

@interface Subject (CoreDataGeneratedAccessors)

- (void)addClass_Object:(NSManagedObject *)value;
- (void)removeClass_Object:(NSManagedObject *)value;
- (void)addClass_:(NSSet *)values;
- (void)removeClass_:(NSSet *)values;

- (void)addProfessorObject:(Professor *)value;
- (void)removeProfessorObject:(Professor *)value;
- (void)addProfessor:(NSSet *)values;
- (void)removeProfessor:(NSSet *)values;

- (void)addTaskObject:(Task *)value;
- (void)removeTaskObject:(Task *)value;
- (void)addTask:(NSSet *)values;
- (void)removeTask:(NSSet *)values;

@end
