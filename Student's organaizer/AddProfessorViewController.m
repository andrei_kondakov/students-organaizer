//
//  AddProfessorViewController.m
//  Student's organaizer
//
//  Created by Andrei on 04.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "AddProfessorViewController.h"
#import "Professor.h"
#import "DataManager.h"

@interface AddProfessorViewController ()

@end

@implementation AddProfessorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fullName.delegate = self;
    self.email.delegate = self;
    self.phone.delegate = self;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)save:(id)sender {

    if (self.fullName.text.length){
        NSLog(@"fullName:\t%@", self.fullName.text);
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Поле ФИО должно быть заполнено!"
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    Professor* professor = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
                                                         inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    professor.name = self.fullName.text;

    if (self.phone.text.length){
        NSLog(@"phone:\t%@", self.phone.text);
        professor.phone = self.phone.text;
    }
    if (self.email.text.length){
        NSLog(@"email:\t%@", self.email.text);
        professor.email = self.email.text;
    }
    [[DataManager sharedManager] saveContext];
    NSLog(@"Saved:\t%@", [professor description]);
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
//- (IBAction)save:(id)sender{
//    NSLog(@"SAVED");
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
