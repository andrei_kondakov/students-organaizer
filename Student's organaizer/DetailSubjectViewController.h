//
//  DetailSubjectViewController.h
//  Student's organaizer
//
//  Created by Andrei on 05.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "ViewController.h"
#import <CoreData/CoreData.h>
@class Subject;
@interface DetailSubjectViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITableView *tableViewProfessors;
@property (weak, nonatomic) Subject* subject;

@property (weak, nonatomic) IBOutlet UIButton *choseProfessorButton;
@property (assign, nonatomic) BOOL addMode;

@end
