//
//  Task.h
//  Student's organaizer
//
//  Created by Andrei on 12.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Subject;

@interface Task : NSManagedObject

@property (nonatomic, retain) NSNumber * done;
@property (nonatomic, retain) NSNumber * priority;
@property (nonatomic, retain) NSDate * endTime;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * taskName;
@property (nonatomic, retain) Subject *subject;

@end
