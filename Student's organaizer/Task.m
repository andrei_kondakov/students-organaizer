//
//  Task.m
//  Student's organaizer
//
//  Created by Andrei on 12.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "Task.h"
#import "Subject.h"


@implementation Task

@dynamic done;
@dynamic priority;
@dynamic endTime;
@dynamic text;
@dynamic taskName;
@dynamic subject;

@end
