//
//  SummariesViewController.m
//  Student's organaizer
//
//  Created by Andrei on 27.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "SummariesViewController.h"
#import "SWRevealViewController.h"
#import "Content.h"
#import "SummaryTableCell.h"
#import "TaskTableCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "JTSImageViewController.h"
#import "JTSImageInfo.h"
@interface SummariesViewController ()

@end

@implementation SummariesViewController
@synthesize fetchedResultsController = _fetchedResultsController;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem.title = @"Изменить";

    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSFetchedResultsController*)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"Content"
                inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:description];
    
    NSSortDescriptor* nameDescription =
    [[NSSortDescriptor alloc] initWithKey:@"node" ascending:YES];

    [fetchRequest setSortDescriptors:@[nameDescription]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Content *content = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text=content.node;
    cell.accessoryType = UITableViewCellAccessoryNone;
    //UIImage *theImage = [UIImage imageNamed:@"menu.png"];
    //cell.imageView.image = theImage;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    SummarylTableViewCell *cell = (SummarylTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"SummaryCell"];
//    if (cell==nil){
//        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SummarylTableViewCell" owner:self options:nil];
//        cell = [nib objectAtIndex:0];
//    }
    SummaryTableCell *cell = (SummaryTableCell*)[tableView dequeueReusableCellWithIdentifier:@"SummaryTableCell"];
    if (cell==nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SummaryTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    Content* content=[self.fetchedResultsController objectAtIndexPath:indexPath];
    NSLog(@"Node:\t%@\nSubjectName:\t%@\nImageUrl:\t%@", content.node, content.subjectName, content.url);

    if (content.node.length!=0){
        cell.nodeLabel.text=content.node;
    }else{
        cell.nodeLabel.text=@"Без названия";
    }
    if (content.subjectName.length!=0){
        cell.subjectNameLabel.text=content.subjectName;
    }else{
        cell.subjectNameLabel.text=@"";
    }
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:[NSURL URLWithString:content.url] resultBlock:^(ALAsset *asset) {
        cell.previewImageView.image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage]];
    } failureBlock:^(NSError *error) {
        NSLog(@"Error");
    }];
//    NSLog(@"%@", cell.nodeLabel);
//    cell.SubjectNameLabel.text;
    //cell.nodeLabel.text=@"TEST123";

//    cell.nodeLabel.text = content.node;
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
   // [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Content* content=[self.fetchedResultsController objectAtIndexPath:indexPath];

    /*    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
     imageInfo.image = self.imageView.image;
     imageInfo.referenceRect = self.imageView.frame;
     imageInfo.referenceView = self.imageView.superview;
     
     // Setup view controller
     JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
     initWithImageInfo:imageInfo
     mode:JTSImageViewControllerMode_Image
     backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
     
     // Present the view controller.
     [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];
*/
    JTSImageInfo *imageInfo = [[JTSImageInfo alloc] init];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:[NSURL URLWithString:content.url] resultBlock:^(ALAsset *asset) {
        UIImage* curImage =[UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage]];
        imageInfo.image = curImage;
//        imageInfo.referenceRect = cell.previewImageView.frame;
  //      imageInfo.referenceView = self.imageView.superview;
    //    cell.previewImageView.image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage]];
        // Setup view controller
        JTSImageViewController *imageViewer = [[JTSImageViewController alloc]
                                               initWithImageInfo:imageInfo
                                               mode:JTSImageViewControllerMode_Image
                                               backgroundStyle:JTSImageViewControllerBackgroundOption_Scaled];
        
        // Present the view controller.
        [imageViewer showFromViewController:self transition:JTSImageViewControllerTransition_FromOriginalPosition];

    } failureBlock:^(NSError *error) {
        NSLog(@"Error");
    }];

    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
