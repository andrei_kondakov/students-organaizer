//
//  SubjectsViewController.m
//  Student's organaizer
//
//  Created by Andrei on 05.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "SubjectsViewController.h"
#import "DetailSubjectViewController.h"
#import "Subject.h"
#import "SWRevealViewController.h"
#import "DataManager.h"

@interface SubjectsViewController ()

@end

@implementation SubjectsViewController
@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem.title = @"Изменить";
    
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}
-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    
    if (editing){
        self.navigationItem.leftBarButtonItem.title = @"Готово";
    }else{
        self.navigationItem.leftBarButtonItem.title = @"Изменить";
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSFetchedResultsController*)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"Subject"
                inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:description];
    
    NSSortDescriptor* nameDescription =
    [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[nameDescription]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    Subject *subject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = subject.name;
//    cell.detailTextLabel.text = @"3";
    cell.accessoryType = UITableViewCellAccessoryNone;
    //UIImage *theImage = [UIImage imageNamed:@"menu.png"];
    //cell.imageView.image = theImage;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailSubjectViewController* vc = (DetailSubjectViewController*)[[self storyboard] instantiateViewControllerWithIdentifier:@"detailSubject"];
    Subject *subject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    vc.subject = subject;
    [self.navigationController pushViewController:vc animated:YES];    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)actionAddButton:(id)sender {
    DetailSubjectViewController* vc = (DetailSubjectViewController*)[[self storyboard] instantiateViewControllerWithIdentifier:@"detailSubject"];
    vc.addMode = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
