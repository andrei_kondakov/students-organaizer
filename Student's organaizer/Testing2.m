//
//  Testing.m
//  Student's organaizer
//
//  Created by Andrei on 06.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "Testing2.h"
#import "DataManager.h"
#import "Professor.h"
#import "Subject.h"
#import "Semester.h"
#import "Task.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"

static NSString* firstNames[] = {
    @"Tran", @"Lenore", @"Bud", @"Fredda", @"Katrice",
    @"Clyde", @"Hildegard", @"Vernell", @"Nellie", @"Rupert",
    @"Billie", @"Tamica", @"Crystle", @"Kandi", @"Caridad",
    @"Vanetta", @"Taylor", @"Pinkie", @"Ben", @"Rosanna",
    @"Eufemia", @"Britteny", @"Ramon", @"Jacque", @"Telma",
    @"Colton", @"Monte", @"Pam", @"Tracy", @"Tresa",
    @"Willard", @"Mireille", @"Roma", @"Elise", @"Trang",
    @"Ty", @"Pierre", @"Floyd", @"Savanna", @"Arvilla",
    @"Whitney", @"Denver", @"Norbert", @"Meghan", @"Tandra",
    @"Jenise", @"Brent", @"Elenor", @"Sha", @"Jessie"
};

static NSString* lastNames[] = {
    
    @"Farrah", @"Laviolette", @"Heal", @"Sechrest", @"Roots",
    @"Homan", @"Starns", @"Oldham", @"Yocum", @"Mancia",
    @"Prill", @"Lush", @"Piedra", @"Castenada", @"Warnock",
    @"Vanderlinden", @"Simms", @"Gilroy", @"Brann", @"Bodden",
    @"Lenz", @"Gildersleeve", @"Wimbish", @"Bello", @"Beachy",
    @"Jurado", @"William", @"Beaupre", @"Dyal", @"Doiron",
    @"Plourde", @"Bator", @"Krause", @"Odriscoll", @"Corby",
    @"Waltman", @"Michaud", @"Kobayashi", @"Sherrick", @"Woolfolk",
    @"Holladay", @"Hornback", @"Moler", @"Bowles", @"Libbey",
    @"Spano", @"Folson", @"Arguelles", @"Burke", @"Rook"
};

//250
//16:51:19.354
//16:51:21.680
//500

@implementation Testing2

+ (void) deleteAllObjects: (NSString *) entityDescription  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [[[DataManager sharedManager] managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    
    
    for (NSManagedObject *managedObject in items) {
        [[[DataManager sharedManager] managedObjectContext] deleteObject:managedObject];
       // NSLog(@"%@ object deleted",entityDescription);
    }
    if (![[[DataManager sharedManager] managedObjectContext] save:&error]) {
        NSLog(@"Error deleting %@ - error:%@",entityDescription,error);
    }
    
}

+(void)start{
    
    NSString* str;
    int count = 1000;
    int countTotal = 401;
    for (int j = 0 ; j< countTotal; j+=100){
        count=j;
        printf("%d;", j);
        [self deleteAllObjects:@"Professor"];


            for (int i=0; i<count; i++){
              
                Professor* professor = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
                                                                     inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
                //str =[[firstNames[arc4random_uniform(50)] stringByAppendingString:@" "]
                   //   stringByAppendingString:lastNames[arc4random_uniform(50)]];
                professor.name = @"aaaa";
            }
            NSDate *methodStart1 = [NSDate date];
        [[DataManager sharedManager] saveContext];
        NSDate *methodFinish1 = [NSDate date];
        NSTimeInterval executionTime1 = [methodFinish1 timeIntervalSinceDate:methodStart1];
//        NSLog(@"executionTime1 = %f", executionTime1);
        printf("%f;", executionTime1);
        
        NSLog(@"%@", [[[DataManager sharedManager] dbURL]absoluteString]);
        FMDatabase *db = [FMDatabase databaseWithPath:[[[DataManager sharedManager] dbURL]absoluteString]];
        BOOL flag = [db open];
    //    NSLog(flag ? @"Yes" : @"No");


        NSString *query = @"delete from zprofessor;";
        //[NSString stringWithFormat:@"insert into zprofessor values (%d, %d, %d, \"%@\", \"%@\", \"%@\");", 1,7,1, @"", @"Fredda Yocum", @""];
        
       // NSLog(@"%@", query);
        
        [db executeUpdate:query];
        query = @"";
//        NSInteger count1 = [db intForQuery:@"SELECT COUNT(zname) FROM ZPROFESSOR"];
        NSInteger count1= 1000;
        

        for (int i=0; i<count; i++){
            //str =[[firstNames[arc4random_uniform(50)] stringByAppendingString:@" "]
              //    stringByAppendingString:lastNames[arc4random_uniform(50)]];
            
            query = [query stringByAppendingString:[NSString stringWithFormat:@" insert into zprofessor values (%d, %d, %d, \"%@\", \"%@\", \"%@\");", count1++,7,1, @"", @"aaaa", @""]];
    //        query =

        }
        NSDate *methodStart2 = [NSDate date];
        [db executeStatements:query];
        NSDate *methodFinish2 = [NSDate date];
        NSTimeInterval executionTime2 = [methodFinish2 timeIntervalSinceDate:methodStart2];
        //NSLog(@"executionTime2 = %f", executionTime2);
                printf("%f\n", executionTime2);
    //    NSLog(@"%@", query);

        
        NSInteger count2 = [db intForQuery:@"SELECT COUNT(zname) FROM ZPROFESSOR"];
    //    NSLog(@"after = %d", count2);

        //insert into zprofessor values (1,7,1,"","Fredda Yocum","")
        [db close];
    }
    
}


@end
