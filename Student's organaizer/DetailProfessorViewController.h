//
//  DetailProfessorViewController.h
//  Student's organaizer
//
//  Created by Andrei on 04.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "ViewController.h"
@class Professor;
@interface DetailProfessorViewController : UIViewController <UITextFieldDelegate,UITableViewDelegate, UITableViewDataSource>

/*
 @dynamic email;
 @dynamic name;
 @dynamic phone;
 @dynamic subject;

 */
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *middleName;
@property (weak, nonatomic) IBOutlet UITextField *surname;
@property (weak, nonatomic) Professor* professor;
@property (weak, nonatomic) IBOutlet UITableView *tableViewSubjects;
@property (weak, nonatomic) IBOutlet UIButton *phoneCallButton;

@end
