//
//  Content.h
//  Student's organaizer
//
//  Created by Andrei on 27.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Summary;

@interface Content : NSManagedObject

@property (nonatomic, retain) NSString * node;
@property (nonatomic, retain) NSString * subjectName;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) Summary *summary;

@end
