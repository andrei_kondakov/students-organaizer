//
//  DetailSubjectViewController.m
//  Student's organaizer
//
//  Created by Andrei on 05.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "ProfessorsViewController.h"
#import "DetailSubjectViewController.h"
#import "Subject.h"
#import "DataManager.h"
#import "Professor.h"

@interface DetailSubjectViewController ()
@property(strong, nonatomic) NSSet* professors;
@property(strong, nonatomic) NSSet* professorsReserv;
@end

@implementation DetailSubjectViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.name.delegate=self;
    if (self.addMode){
        Subject *subject = [NSEntityDescription insertNewObjectForEntityForName:@"Subject"
                                                         inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
        self.subject = subject;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Отмена"
                                                                                 style:UIBarButtonItemStyleBordered
                                                                                target:self
                                                                                action:@selector(actionBackButton:)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить"
                                                                                  style:UIBarButtonItemStyleBordered
                                                                                 target:self
                                                                                 action:@selector(save:)];
        self.name.enabled=YES;
        self.choseProfessorButton.hidden=NO;

        //[[UIBarButtonItem alloc]initWithTitle:@"Назад"
          //                                                                                                              style:UIBarButtonItemStyleBordered target:self action:@selector(backAction:)];
    }else{
        self.navigationItem.rightBarButtonItem = self.editButtonItem;
        self.name.text = self.subject.name;

        self.professors = self.subject.professor;  // self.subject.professor - NSSet of professors in Core Data
        self.choseProfessorButton.hidden=YES;
        self.name.enabled=NO;
    }
    
    
//    NSLog(@"%@ %d", [self.subject description], professors.count);
    
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
   // [[[DataManager sharedManager] managedObjectContext] refreshObject:self.subject mergeChanges:NO];
    self.professors = self.subject.professor;  // self.subject.professor - NSSet of professors in Core Data
    [[self tableViewProfessors] reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    if (editing == YES){
        self.name.enabled=YES;
        NSLog(@"Starting editing");
        self.choseProfessorButton.hidden = NO;
    }else{
        self.name.enabled=NO;
        self.choseProfessorButton.hidden = YES;
        [[DataManager sharedManager] saveContext];
        NSLog(@"Ending editing");
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField
     resignFirstResponder];
    return YES;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.professors.count){
        return self.professors.count;
    }else{
        return 1;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Преподователи";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"profCell"];
    if (self.professors.count){
        Professor* professor = (Professor*)[[self.professors allObjects] objectAtIndex:indexPath.row];
        cell.textLabel.text = professor.name;
    }else{
        cell.textLabel.text = @"none";
    }
    return cell;
}


#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)actionBackButton:(id)sender {
    if (self.editing){
        [[[DataManager sharedManager] managedObjectContext] refreshObject:self.subject mergeChanges:NO];
    }
    if (self.addMode){
        [[[DataManager sharedManager] managedObjectContext] deleteObject:self.subject];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)actionEditProffessorsButton:(id)sender {
    ProfessorsViewController *vc = (ProfessorsViewController*)[[self storyboard] instantiateViewControllerWithIdentifier:@"listProfessors"];
    vc.chooseMode=YES;
    vc.subject=self.subject;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)save:(id)sender {
    
    if (self.name.text.length){
        NSLog(@"name:\t%@", self.name.text);
        self.subject.name = self.name.text;
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Поле \"Название предмета\" должно быть заполнено!"
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    [[DataManager sharedManager] saveContext];
    NSLog(@"Saved:\t%@", [self.subject description]);
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
