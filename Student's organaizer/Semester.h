//
//  Semester.h
//  Student's organaizer
//
//  Created by Andrei on 12.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Subject;

@interface Semester : NSManagedObject

@property (nonatomic, retain) NSDate * startTime;
@property (nonatomic, retain) NSSet *subject;
@end

@interface Semester (CoreDataGeneratedAccessors)

- (void)addSubjectObject:(Subject *)value;
- (void)removeSubjectObject:(Subject *)value;
- (void)addSubject:(NSSet *)values;
- (void)removeSubject:(NSSet *)values;

@end
