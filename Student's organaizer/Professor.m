//
//  Professor.m
//  Student's organaizer
//
//  Created by Andrei on 12.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "Professor.h"
#import "Subject.h"


@implementation Professor

@dynamic email;
@dynamic name;
@dynamic phone;
@dynamic subject;

@end
