//
//  ProfessorsViewController.m
//  Student's organaizer
//
//  Created by Andrei on 04.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "ProfessorsViewController.h"
#import "CoreDataViewController.h"
#import "Professor.h"
#import "DetailProfessorViewController.h"
#import "AddProfessorViewController.h"
#import "Subject.h"
#import "DataManager.h"

@interface ProfessorsViewController ()

@end

@implementation ProfessorsViewController
@synthesize fetchedResultsController = _fetchedResultsController;



- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Преподователи";
    if (!self.chooseMode){
        self.navigationItem.leftBarButtonItem = self.editButtonItem;
        self.navigationItem.leftBarButtonItem.title = @"Изменить";
    }else{
        self.navigationItem.rightBarButtonItem = nil;
        self.tableView.allowsMultipleSelection = YES;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Назад"
                                                                                 style:UIBarButtonItemStyleBordered
                                                                                target:self
                                                                                action:@selector(backAction:)];
    }
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    if (self.chooseMode){
//        [[[DataManager sharedManager] managedObjectContext] refreshObject:self.subject mergeChanges:NO];
//    }
}
-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    
    if (editing){
        self.navigationItem.leftBarButtonItem.title = @"Готово";
    }else{
        self.navigationItem.leftBarButtonItem.title = @"Изменить";
    }
}

- (IBAction)backAction:(id)sender {
    // Just for chose mod
    [self.navigationController popViewControllerAnimated:YES];
}


//- (void)insertNewObject:(id)sender {
//    AddProfessorViewController *vc = (AddProfessorViewController*)[[self storyboard] instantiateViewControllerWithIdentifier:@"addProfessor"];
 //   //    DetailProfessorViewController* vc = [[DetailProfessorViewController alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSFetchedResultsController*)fetchedResultsController{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"Professor"
                inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:description];
    
    NSSortDescriptor* nameDescription =
    [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[nameDescription]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    Professor *professor = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = professor.name;
    if (professor.email){
        cell.detailTextLabel.text = professor.email;
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
    if (self.chooseMode) {
        if ([professor.subject containsObject:self.subject]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }

    //UIImage *theImage = [UIImage imageNamed:@"menu.png"];
    //cell.imageView.image = theImage;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Professor *professor = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (self.chooseMode){
        UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType=UITableViewCellAccessoryCheckmark;
        [professor addSubjectObject:self.subject];
    }else{
        DetailProfessorViewController *vc = (DetailProfessorViewController*)[[self storyboard] instantiateViewControllerWithIdentifier:@"detailProfessor"];
        vc.professor=professor;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.chooseMode){
        UITableViewCell *cell=[self.tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType=UITableViewCellAccessoryNone;
        Professor *professor = [self.fetchedResultsController objectAtIndexPath:indexPath];
        [professor removeSubjectObject:self.subject];
    }else{
        [super tableView:tableView didDeselectRowAtIndexPath:indexPath];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
