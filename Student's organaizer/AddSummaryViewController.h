//
//  AddSummaryViewController.h
//  Student's organaizer
//
//  Created by Andrei on 27.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddSummaryViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *descriptionLabel;
@property (weak, nonatomic) IBOutlet UITextField *subjectNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;

@end
