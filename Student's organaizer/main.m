//
//  main.m
//  Student's organaizer
//
//  Created by Andrei on 27.04.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
