//
//  SummariesViewController.h
//  Student's organaizer
//
//  Created by Andrei on 27.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataViewController.h"

@interface SummariesViewController : CoreDataViewController

@end
