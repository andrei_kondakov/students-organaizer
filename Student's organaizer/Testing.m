//
//  Testing.m
//  Student's organaizer
//
//  Created by Andrei on 06.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "Testing.h"
#import "DataManager.h"
#import "Professor.h"
#import "Subject.h"
#import "Semester.h"
#import "Task.h"

static NSString* firstNames[] = {
    @"Tran", @"Lenore", @"Bud", @"Fredda", @"Katrice",
    @"Clyde", @"Hildegard", @"Vernell", @"Nellie", @"Rupert",
    @"Billie", @"Tamica", @"Crystle", @"Kandi", @"Caridad",
    @"Vanetta", @"Taylor", @"Pinkie", @"Ben", @"Rosanna",
    @"Eufemia", @"Britteny", @"Ramon", @"Jacque", @"Telma",
    @"Colton", @"Monte", @"Pam", @"Tracy", @"Tresa",
    @"Willard", @"Mireille", @"Roma", @"Elise", @"Trang",
    @"Ty", @"Pierre", @"Floyd", @"Savanna", @"Arvilla",
    @"Whitney", @"Denver", @"Norbert", @"Meghan", @"Tandra",
    @"Jenise", @"Brent", @"Elenor", @"Sha", @"Jessie"
};

static NSString* lastNames[] = {
    
    @"Farrah", @"Laviolette", @"Heal", @"Sechrest", @"Roots",
    @"Homan", @"Starns", @"Oldham", @"Yocum", @"Mancia",
    @"Prill", @"Lush", @"Piedra", @"Castenada", @"Warnock",
    @"Vanderlinden", @"Simms", @"Gilroy", @"Brann", @"Bodden",
    @"Lenz", @"Gildersleeve", @"Wimbish", @"Bello", @"Beachy",
    @"Jurado", @"William", @"Beaupre", @"Dyal", @"Doiron",
    @"Plourde", @"Bator", @"Krause", @"Odriscoll", @"Corby",
    @"Waltman", @"Michaud", @"Kobayashi", @"Sherrick", @"Woolfolk",
    @"Holladay", @"Hornback", @"Moler", @"Bowles", @"Libbey",
    @"Spano", @"Folson", @"Arguelles", @"Burke", @"Rook"
};


@implementation Testing

+(void)start{
//    for (int i=0; i<10; i++){
//        Professor* professor = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
//                                                             inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
//        professor.name = [[firstNames[arc4random_uniform(50)] stringByAppendingString:@" "]
//                          stringByAppendingString:lastNames[arc4random_uniform(50)]];
//
//    }
    Professor* professor1 = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
                                                         inManagedObjectContext:[[DataManager sharedManager]managedObjectContext]];
    professor1.name = @"Сергей Юрьевич Скоробогатов";
    professor1.phone = @"8-910-406-7925";
    Professor* professor2 = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
                                                          inManagedObjectContext:[[DataManager sharedManager]managedObjectContext]];

    professor2.name = @"Игорь Эдуардович Вишняков";
    professor2.email = @"vscie@yandex.ru";
    

    Professor* professor3 = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
                                                          inManagedObjectContext:[[DataManager sharedManager]managedObjectContext]];
    professor3.name = @"Александр Коновалов";
    professor3.email = @"a.v.konovalov87@mail.ru";
    
    Professor* professor4 = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
                                                          inManagedObjectContext:[[DataManager sharedManager]managedObjectContext]];
    professor4.name = @"Анатолий Николаевич Канатников";
    
    Professor* professor5 = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
                                                          inManagedObjectContext:[[DataManager sharedManager]managedObjectContext]];
    professor5.name = @"Павел Александрович Власов";
    professor5.email = @"pvlx@mail.ru";
    
    Subject* subject1 = [NSEntityDescription insertNewObjectForEntityForName:@"Subject"
                                                      inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    subject1.name = @"ОФП - язык Scala";
    [subject1 addProfessorObject:professor1];
    Subject* subject2 = [NSEntityDescription insertNewObjectForEntityForName:@"Subject"
                                                      inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    
    subject2.name = @"Дифференциальные уравнения";
    [subject2 addProfessorObject:professor4];
    [subject2 addProfessorObject:professor5];
    
    Subject* subject3 = [NSEntityDescription insertNewObjectForEntityForName:@"Subject"
                                                      inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    
    subject3.name = @"Компиляторы";
    [subject3 addProfessorObject:professor1];
    [subject3 addProfessorObject:professor3];

    
    Subject* subject4 = [NSEntityDescription insertNewObjectForEntityForName:@"Subject"
                                                      inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    
    subject4.name = @"Курсовой проект";
    [subject4 addProfessorObject:professor2];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   [dateFormatter setDateFormat:@"dd-MM-yyyy"];

    Task* task1 = [NSEntityDescription insertNewObjectForEntityForName:@"Task"
                                                inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    task1.taskName = @"Лабораторная работа №8";
    task1.text = @"Множества FIRST для РБНФ";
    task1.subject = subject3;
    NSString *endTime = @"17-05-2015";
    task1.endTime = [dateFormatter dateFromString:endTime];
    task1.done=[NSNumber numberWithBool:NO];

    Task* task2 = [NSEntityDescription insertNewObjectForEntityForName:@"Task"
                                                inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    task2.taskName = @"Исправить РК№1";
    task2.text = @"Исправить номера №1-3 в РК№1";
    task2.subject = subject2;
    NSString *endTime1 = @"24-05-2015";
    task2.endTime = [dateFormatter dateFromString:endTime1];
    task2.done=[NSNumber numberWithBool:NO];
    
    Task* task3 = [NSEntityDescription insertNewObjectForEntityForName:@"Task"
                                                inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    task3.taskName = @"Типовой расчет №1";
    task3.text = @"Сделать типовик №1";
    task3.subject = subject2;
    NSString *endTime3 = @"12-05-2015";
    task3.endTime = [dateFormatter dateFromString:endTime3];
    task3.done=[NSNumber numberWithBool:YES];
    
    

    Task* task4 = [NSEntityDescription insertNewObjectForEntityForName:@"Task"
                                                inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    task4.taskName = @"Раздел \"Задачи\"";
    task4.text = @"Добавить секции приоритетов";
    task4.subject = subject4;
    NSString *endTime4 = @"12-05-2015";
    task4.endTime = [dateFormatter dateFromString:endTime4];
    task4.done=[NSNumber numberWithBool:NO];
    
    Task* task5 = [NSEntityDescription insertNewObjectForEntityForName:@"Task"
                                                inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    task5.taskName = @"Добавить расписание";
    task5.text = @"Реализовать раздел \"Расписание\"";
    task5.subject = subject4;
    NSString *endTime5 = @"12-05-2015";
    task5.endTime = [dateFormatter dateFromString:endTime5];
    task5.done=[NSNumber numberWithBool:NO];
    
    
    
    
    [[DataManager sharedManager] saveContext];
}

@end
