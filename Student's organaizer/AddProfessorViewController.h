//
//  AddProfessorViewController.h
//  Student's organaizer
//
//  Created by Andrei on 04.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "ViewController.h"

@interface AddProfessorViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *fullName;

@end
