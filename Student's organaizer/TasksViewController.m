//
//  TasksVIewController.m
//  Student's organaizer
//
//  Created by Andrei on 11.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "TasksViewController.h"
#import "SWRevealViewController.h"
#import "DataManager.h"
#import "Task.h"
#import "Subject.h"
#import "DetailTaskTableViewController.h"
#import "TaskTableCell.h"

@interface TasksViewController ()
@property (assign, nonatomic) NSInteger selectedSegment;
@end

@implementation TasksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem.title = @"Изменить";
    //    self.menuBarItem.target = self.revealViewController;
    //    self.menuBarItem.action = @selector(revealToggle:);
    //[[UINavigationBar appearance] setBarTintColor:[UIColor yellowColor]];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"TaskCell"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    
    if (editing){
        self.navigationItem.leftBarButtonItem.title = @"Готово";
    }else{
        self.navigationItem.leftBarButtonItem.title = @"Изменить";
    }
}

-(NSManagedObjectContext*) managedObjectContext {
    if (!_managedObjectContext){
        _managedObjectContext = [[DataManager sharedManager] managedObjectContext];
    }
    return _managedObjectContext;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (tableView==self.tasksTableView){
        return [[self.fetchedResultsController sections] count];
    }
    return [super numberOfSectionsInTableView:tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==self.tasksTableView){
        id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
        return [sectionInfo numberOfObjects];
    }
    return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==self.tasksTableView){
        //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TaskCell" forIndexPath:indexPath];
        //TaskTableCell *cell = (TaskTableCell*)[tableView dequeueReusableCellWithIdentifier:@"TaskCell"];
        //        TaskTableCell *cell = [[TaskTableCell alloc] init];
        //        SimpleTableCell *cell = (SimpleTableCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        //        if (cell == nil)
        //        {
        //            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableCell" owner:self options:nil];
        //            cell = [nib objectAtIndex:0];
        //        }
        //
        //        cell.nameLabel.text = [tableData objectAtIndex:indexPath.row];
        //        cell.thumbnailImageView.image = [UIImage imageNamed:[thumbnails objectAtIndex:indexPath.row]];
        //        cell.prepTimeLabel.text = [prepTime objectAtIndex:indexPath.row];
        //
        TaskTableCell *cell = (TaskTableCell*)[tableView dequeueReusableCellWithIdentifier:@"TaskTable"];
        if (cell==nil){
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TaskTableCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        Task *task = [self.fetchedResultsController objectAtIndexPath:indexPath];
        
        cell.SubjectNameLabel.text = task.subject.name;
        cell.TaskTextLabel.text = task.taskName;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        
        
        cell.DataLabel.text = [dateFormatter stringFromDate:task.endTime];;
        if ([task.priority integerValue]==0){
            cell.PriorityLabel.text=@"";
        }else{
            NSLog(@">>>%d",  [task.priority integerValue]);
        }
        
        if ([task.done isEqual:@1]){
            cell.TaskTextLabel.backgroundColor=[UIColor colorWithRed:0.0f green:255.0f blue:0.0f alpha:0.4f];
        }else{
            cell.TaskTextLabel.backgroundColor=[UIColor colorWithRed:255.0f green:0.0f blue:0.0f alpha:0.4f];
        }
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        
        return cell;
    }
    return [super tableView:tableView cellForRowAtIndexPath:indexPath];
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (tableView==self.tasksTableView){
        return @"";
    }
    return [super tableView:tableView titleForHeaderInSection:section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==self.tasksTableView){
        if (editingStyle == UITableViewCellEditingStyleDelete) {
            NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
            [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
            
            NSError *error = nil;
            if (![context save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                abort();
            }
        }
        return;
    }
    [super tableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Task *task = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if ([cell isKindOfClass:[TaskTableCell class]]){
        NSLog(@"YES");
    }
    
    cell.detailTextLabel.text = task.taskName;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", [dateFormatter stringFromDate:task.endTime], task.subject.name];
    cell.textLabel.text = task.subject.name;
    if ([task.done isEqual:@1]){
        cell.detailTextLabel.backgroundColor=[UIColor colorWithRed:0.0f green:255.0f blue:0.0f alpha:0.4f];
    }else{
        cell.detailTextLabel.backgroundColor=[UIColor colorWithRed:255.0f green:0.0f blue:0.0f alpha:0.4f];
    }
    cell.accessoryType = UITableViewCellAccessoryNone;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.tasksTableView==tableView){
        DetailTaskTableViewController* vc = (DetailTaskTableViewController*)[[self storyboard] instantiateViewControllerWithIdentifier:@"detailTask"];
        Task *task = [self.fetchedResultsController objectAtIndexPath:indexPath];
        vc.task = task;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Task" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"endTime" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSLog(@"selectedSegment:\t%d", self.selectedSegment);
    if (self.selectedSegment==1) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"done = 0"];
        [fetchRequest setPredicate:predicate];
    }else if (self.selectedSegment==2){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"done = 1"];
        [fetchRequest setPredicate:predicate];
    }
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tasksTableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tasksTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tasksTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tasksTableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tasksTableView endUpdates];
}


- (IBAction)segmentSwitch:(id)sender {
    self.selectedSegment = self.taskTypeSegmentedControl.selectedSegmentIndex;
    self.fetchedResultsController = nil;
    [self.tableView reloadData];
}


@end
