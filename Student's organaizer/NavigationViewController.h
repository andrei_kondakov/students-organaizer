//
//  NavigationViewController.h
//  Student's organaizer
//
//  Created by Andrei on 04.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;

@end
