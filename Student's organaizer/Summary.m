//
//  Summary.m
//  Student's organaizer
//
//  Created by Andrei on 27.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "Summary.h"
#import "Subject.h"


@implementation Summary

@dynamic content;
@dynamic subject;

@end
