//
//  Professor.h
//  Student's organaizer
//
//  Created by Andrei on 12.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Subject;

@interface Professor : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSSet *subject;
@end

@interface Professor (CoreDataGeneratedAccessors)

- (void)addSubjectObject:(Subject *)value;
- (void)removeSubjectObject:(Subject *)value;
- (void)addSubject:(NSSet *)values;
- (void)removeSubject:(NSSet *)values;

@end
