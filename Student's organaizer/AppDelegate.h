//
//  AppDelegate.h
//  Student's organaizer
//
//  Created by Andrei on 27.04.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end

