//
//  Subject.m
//  Student's organaizer
//
//  Created by Andrei on 12.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "Subject.h"
#import "Professor.h"
#import "Semester.h"
#import "Task.h"


@implementation Subject

@dynamic name;
@dynamic class_;
@dynamic professor;
@dynamic semester;
@dynamic task;

@end
