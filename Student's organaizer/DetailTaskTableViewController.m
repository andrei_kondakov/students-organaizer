//
//  DetailTaskTableViewController.m
//  Student's organaizer
//
//  Created by Andrei on 12.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "DetailTaskTableViewController.h"
#import "Task.h"
#import "Subject.h"

@interface DetailTaskTableViewController ()
@property(assign, nonatomic) BOOL shouldTextFieldsAllowEditing;
@end

@implementation DetailTaskTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.rightBarButtonItem.title = @"Изменить";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Назад"
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:self action:@selector(actionBackButton:)];
    self.navigationItem.title = self.task.taskName;
    self.subjectName.text = self.task.subject.name;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    self.endTime.text = [dateFormatter stringFromDate:self.task.endTime];
    if (self.task.priority){
        self.priority.selectedSegmentIndex=self.task.priority;
    }else{
        self.priority.selectedSegmentIndex=0;
    }
    self.taskText.text = self.task.text;
    self.shouldTextFieldsAllowEditing=NO;
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", [dateFormatter stringFromDate:task.endTime], task.subject.name];

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    
    if (editing){
        self.shouldTextFieldsAllowEditing=YES;
        self.navigationItem.rightBarButtonItem.title = @"Готово";
    }else{
        self.shouldTextFieldsAllowEditing=NO;
        self.navigationItem.rightBarButtonItem.title = @"Изменить";
    }
}

#pragma mark - Table view data source
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 1;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
#pragma mark - UITextFieldDelegate
// Enable/disable all textfields in a viewcontroller
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return self.shouldTextFieldsAllowEditing;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Navigation
 - (IBAction)actionBackButton:(id)sender {
  //   if (self.editing){
     //[[[DataManager sharedManager] managedObjectContext] refreshObject:self.subject mergeChanges:NO];
//     }
//     if (self.addMode){
//     [[[DataManager sharedManager] managedObjectContext] deleteObject:self.subject];
  //   }
        [self.navigationController popViewControllerAnimated:YES];
    // }
}
/*

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
