//
//  ViewController.m
//  Student's organaizer
//
//  Created by Andrei on 27.04.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "ViewController.h"
#import "SWRevealViewController.h" // App menu implementation
#import "DSLCalendarView.h"

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _barButton.target = self.revealViewController;
    _barButton.action = @selector(revealToggle:);
       // [[UINavigationBar appearance] setBarTintColor:[UIColor yellowColor]];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    //self.calendarView.delegate = self;
    
    self.calendar = [JTCalendar new];
    
    self.calendar.calendarAppearance.calendar.firstWeekday = 2; // Monday
    self.calendar.calendarAppearance.ratioContentMenu = 1.;
    self.calendar.calendarAppearance.menuMonthTextColor = [UIColor whiteColor];
    self.calendar.calendarAppearance.dayCircleColorSelected = [UIColor blueColor];
    self.calendar.calendarAppearance.dayTextColorSelected = [UIColor whiteColor];
//    self.calendar.calendarAppearance.isWeekMode = YES;
    
    [self.calendar setMenuMonthsView:self.calendarMenuView];
    [self.calendar setContentView:self.calendarContentView];
    [self.calendar setDataSource:self];
    
    // You don't have to call reloadAppearance
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidLayoutSubviews
{
    [self.calendar repositionViews];
}

- (BOOL)calendarHaveEvent:(JTCalendar *)calendar date:(NSDate *)date
{
    return NO;
}

- (void)calendarDidDateSelected:(JTCalendar *)calendar date:(NSDate *)date
{
    NSLog(@"%@", date);
    
    NSDateFormatter *weekday = [[NSDateFormatter alloc] init] ;
    [weekday setDateFormat: @"EEEE"];
    NSLog(@"The day of the week is: %@", [weekday stringFromDate:date]);
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSWeekdayCalendarUnit fromDate:date];
    
    NSLog(@"%d", [components weekday]);
    
}

@end
