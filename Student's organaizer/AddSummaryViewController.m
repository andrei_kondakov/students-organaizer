//
//  AddSummaryViewController.m
//  Student's organaizer
//
//  Created by Andrei on 27.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "AddSummaryViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Summary.h"
#import "Content.h"
#import "DataManager.h"
#import <libkern/OSAtomic.h>
@interface AddSummaryViewController ()
@property (strong, nonatomic) UIImagePickerController *picker;
@property (strong, nonatomic) NSString* urlImageString;
@end

@implementation AddSummaryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.delegate = self;
    self.picker.allowsEditing = YES;
   
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)takePhotoAction:(id)sender {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        return;
    }
    self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:self.picker animated:YES completion:NULL];
}
- (IBAction)selectPhoto:(id)sender {
    self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.picker animated:YES completion:NULL];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //    self.imageView.image = chosenImage;
    __block NSURL *urlImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];

    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
//
         UIImage* cameraImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        ALAssetsLibrary* assetsLibrary = [[ALAssetsLibrary alloc] init];
        dispatch_async(queue, ^{
       // __block BOOL isFinished;

        [assetsLibrary writeImageToSavedPhotosAlbum:cameraImage.CGImage
                                           metadata:[info objectForKey:UIImagePickerControllerMediaMetadata]
                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                        if (!error) {
                                            urlImage=assetURL;
                                        }
                                    }];

        });
     //   dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);

    }else{
        urlImage = info[UIImagePickerControllerReferenceURL];
    }
    //NSLog(@"%@", [info description]);
    NSLog(@"%@", urlImage);
    self.urlImageString = [urlImage absoluteString];
    NSLog(@"ImageURL=%@", [urlImage absoluteString]);
    //    self.imageView.image = [UIImage imageWithContentsOfFile:[urlImage absoluteString]];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library assetForURL:[NSURL URLWithString:[urlImage absoluteString]] resultBlock:^(ALAsset *asset) {
        self.previewImageView.image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage]];
    } failureBlock:^(NSError *error) {
        NSLog(@"Error");
    }];

    
}
- (IBAction)doneActionButton:(id)sender {
    /*
     
     if (self.fullName.text.length){
     NSLog(@"fullName:\t%@", self.fullName.text);
     }else{
     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
     message:@"Поле ФИО должно быть заполнено!"
     delegate:nil cancelButtonTitle:@"OK"
     otherButtonTitles:nil];
     [alert show];
     return;
     }
     
     Professor* professor = [NSEntityDescription insertNewObjectForEntityForName:@"Professor"
     inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
     professor.name = self.fullName.text;
     
     if (self.phone.text.length){
     NSLog(@"phone:\t%@", self.phone.text);
     professor.phone = self.phone.text;
     }
     if (self.email.text.length){
     NSLog(@"email:\t%@", self.email.text);
     professor.email = self.email.text;
     }
     [[DataManager sharedManager] saveContext];
     NSLog(@"Saved:\t%@", [professor description]);
*/
    if (!self.urlImageString){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                        message:@"Не удалось получить URL изображения"
                                                       delegate:nil cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    Content* content = [NSEntityDescription insertNewObjectForEntityForName:@"Content"
                                                     inManagedObjectContext:[[DataManager sharedManager] managedObjectContext]];
    content.node=self.descriptionLabel.text;
    content.subjectName=self.subjectNameLabel.text;
    content.url=self.urlImageString;
    [[DataManager sharedManager] saveContext];
    NSLog(@"Save content with:");
    NSLog(@"Node:\t%@\nSubjectName:\t%@\nImageUrl:\t%@", self.descriptionLabel.text, self.subjectNameLabel.text, self.urlImageString);
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
