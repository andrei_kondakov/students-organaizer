//
//  Summary.h
//  Student's organaizer
//
//  Created by Andrei on 27.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Subject;

@interface Summary : NSManagedObject

@property (nonatomic, retain) NSSet *content;
@property (nonatomic, retain) Subject *subject;
@end

@interface Summary (CoreDataGeneratedAccessors)

- (void)addContentObject:(NSManagedObject *)value;
- (void)removeContentObject:(NSManagedObject *)value;
- (void)addContent:(NSSet *)values;
- (void)removeContent:(NSSet *)values;

@end
