//
//  ProfessorsViewController.h
//  Student's organaizer
//
//  Created by Andrei on 04.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "CoreDataViewController.h"
@class Subject;
@interface ProfessorsViewController : CoreDataViewController

@property (weak, nonatomic) IBOutlet UIViewController *detailProfessorsController;
@property (strong, nonatomic) IBOutlet Subject *subject;


//properties for chose mode
@property (assign, nonatomic) BOOL chooseMode;
@end
