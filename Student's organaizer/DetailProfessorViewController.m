//
//  DetailProfessorViewController.m
//  Student's organaizer
//
//  Created by Andrei on 04.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import "DetailProfessorViewController.h"
#import "Professor.h"
#import "DataManager.h"
#import "Subject.h"
@interface DetailProfessorViewController ()
@property(strong, nonatomic) NSSet *subjects;
@property(strong, nonatomic) NSArray *fullName;
@property(assign, nonatomic) BOOL shouldTextFieldsAllowEditing;
@end

@implementation DetailProfessorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.rightBarButtonItem.title = @"Изменить";
    self.fullName = [self.professor.name componentsSeparatedByString:@" "];
    
    self.name.text = [self.fullName firstObject];
    if ([self.fullName count]==3){
        self.middleName.text = [self.fullName objectAtIndex:1];
    }
    self.surname.text = [self.fullName lastObject];
    
    if (self.professor.email){
        self.email.text = self.professor.email;
    }
    if (self.professor.phone){
        self.phone.text = self.professor.phone;
    }
    self.subjects = self.professor.subject;
    NSLog(@"Total subject here:\t%d", [self.subjects count]);
}
- (IBAction)callPhoneAction:(id)sender {
        if (self.professor.phone){
            NSString *phNo = self.phone.text;
            NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
    
            if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
                [[UIApplication sharedApplication] openURL:phoneUrl];
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                                message:@"Функция позвонить недоступна!"
                                                               delegate:nil cancelButtonTitle:@"OK"                                  otherButtonTitles:nil];
                [alert show];
            }
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка"
                                                            message:@"Номер не указан!"
                                                           delegate:nil cancelButtonTitle:@"OK"                                  otherButtonTitles:nil];
            [alert show];
        }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    if (editing == YES){
        self.shouldTextFieldsAllowEditing=YES;
        self.navigationItem.rightBarButtonItem.title = @"Готово";
        NSLog(@"Starting editing");
    }else{
        self.shouldTextFieldsAllowEditing=NO;
        self.navigationItem.rightBarButtonItem.title = @"Изменить";
        self.professor.name = [NSString stringWithFormat:@"%@ %@ %@", self.name.text, self.middleName.text, self.surname.text];
        NSLog(@"%@", self.professor.name);
//        if ([self.fullName count]==3){
//            self.professor.name = [self.name.text stringByAppendingString:self.middleName]
//        }
//        self.professor.name = [self.name.text stringByAppendingString:self.]
       // self.professor.name = self.fullName.text;
        self.professor.email = self.email.text;
        self.professor.phone= self.phone.text;
        [[DataManager sharedManager] saveContext];
        NSLog(@"Ending editing");
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

// Enable/disable all textfields in a viewcontroller
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return self.shouldTextFieldsAllowEditing;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.subjects.count){
        return self.subjects.count;
    }else{
        return 1;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"Предметы";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subjectCell"];
    if (self.subjects.count){
        Subject* subject = (Subject*)[[self.subjects allObjects] objectAtIndex:indexPath.row];
        cell.textLabel.text = subject.name;
    }else{
        cell.textLabel.text = @"none";
    }
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)actionBackButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
