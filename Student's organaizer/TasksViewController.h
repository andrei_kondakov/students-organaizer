//
//  TasksVIewController.h
//  Student's organaizer
//
//  Created by Andrei on 11.05.15.
//  Copyright (c) 2015 Andrei Kondakov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface TasksViewController : UITableViewController <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UITableView *tasksTableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *taskTypeSegmentedControl;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *menuBarItem;

@end
